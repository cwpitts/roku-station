#include "rokuchannel.h"

#include <QString>
#include <QMap>

RokuChannel::RokuChannel(QString name)
{
    this->name = name;
}

RokuChannel::RokuChannel(QString name, QMap<QString, QString> attributes)
{
    this->name = name;
    this->attributes = attributes;
}

RokuChannel::RokuChannel(QString name, QMap<QString, QString> attributes, QByteArray icon)
{
    this->name = name;
    this->attributes = attributes;
    this->icon = icon;
}

RokuChannel::RokuChannel()
{}

QString RokuChannel::get_name()
{
    return this->name;
}

QString RokuChannel::get_attribute(QString attributeName)
{
    if (this->attributes.contains(attributeName))
    {
        return this->attributes[attributeName];
    }

    return NULL;
}

QByteArray RokuChannel::get_icon()
{
    return this->icon;
}
