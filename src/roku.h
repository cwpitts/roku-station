#ifndef ROKU_H
#define ROKU_H

#include "rokuchannel.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
#include <QVector>

class Roku : public QObject
{
  Q_OBJECT

 public:
  Roku(QString ip, QString port);
  Roku();
  void add_channel(RokuChannel channel);
  QVector<RokuChannel> get_channels();
  bool has_channel(QString name);
  QString get_ip();
  bool launch(QString name);
  void up();
  void down();
  void left();
  void right();
  void select();
  void character(QString c);
  void back();
  void backspace();
  void pause();
  void rev();
  void fwd();
  void home();

 private:
  QString ip;
  QString port;
  QVector<RokuChannel> channels;
  QNetworkAccessManager qnam;
  QNetworkReply* reply;

  QByteArray download_channel_icon(QString channelID);
  void download_channels();

 private slots:
  void handle_channel_response();
};

#endif  // ROKU_H
