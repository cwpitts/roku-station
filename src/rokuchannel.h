#ifndef ROKUCHANNEL_H
#define ROKUCHANNEL_H

#include <QString>
#include <QMap>
#include <QByteArray>

class RokuChannel
{
public:
    RokuChannel(QString name);
    RokuChannel(QString name, QMap<QString, QString> attributes);
    RokuChannel(QString name, QMap<QString, QString> attributes, QByteArray icon);
    RokuChannel();
    QString get_name();
    QString get_attribute(QString attributeName);
    QByteArray get_icon();

private:
    QString name;
    QMap<QString, QString> attributes;
    QByteArray icon;
};

#endif // ROKUCHANNEL_H
