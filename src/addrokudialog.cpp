#include "addrokudialog.h"
#include "ui_addrokudialog.h"

AddRokuDialog::AddRokuDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddRokuDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Roku Station | Add Roku");
}

AddRokuDialog::~AddRokuDialog()
{
    delete ui;
}

void AddRokuDialog::on_buttonBox_accepted()
{
    this->ip = this->ui->ipAddressField->text();
}

QString AddRokuDialog::get_ip()
{
    return this->ip;
}
