#include "chooserokudialog.h"
#include "ui_chooserokudialog.h"

#include <QDebug>
#include <QListWidgetItem>
#include <QString>

ChooseRokuDialog::ChooseRokuDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::ChooseRokuDialog)
{
  ui->setupUi(this);

  this->setWindowTitle("Roku Station | Choose Roku");
}

ChooseRokuDialog::~ChooseRokuDialog() { delete ui; }

void ChooseRokuDialog::set_roku_list(QList<QString> rokus)
{
  foreach (QString roku, rokus)
  {
    if (!this->rokus.contains(roku))
    {
      this->rokus.push_back(roku);
      this->ui->rokuList->addItem(new QListWidgetItem(roku));
    }
  }
}

void ChooseRokuDialog::on_buttonBox_accepted() { this->accept(); }

void ChooseRokuDialog::on_rokuList_clicked(const QModelIndex& index)
{
  this->currentRoku = this->rokus.at(index.row());
}

QString ChooseRokuDialog::get_chosen_roku() { return this->currentRoku; }

void ChooseRokuDialog::on_rokuList_doubleClicked(const QModelIndex& index)
{
  this->on_rokuList_clicked(index);
  this->accept();
}
