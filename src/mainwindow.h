#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "roku.h"
#include "chooserokudialog.h"
#include "userokudialog.h"
#include "addrokudialog.h"
#include "scandialog.h"

#include <QMainWindow>
#include <QVector>
#include <QNetworkAccessManager>
#include <QNetworkReply>

const QString ROKU_PORT = "8060";

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QMap<QString, Roku*> rokus;
    ChooseRokuDialog* crd;
    AddRokuDialog* ard;
    UseRokuDialog* urd;
    ScanDialog* scnd;



private slots:
    void on_addRokuButton_clicked();
    void on_chooseRokuButton_clicked();
};

#endif // MAINWINDOW_H
