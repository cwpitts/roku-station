#ifndef USEROKUDIALOG_H
#define USEROKUDIALOG_H

#include "roku.h"

#include <QDialog>
#include <QMap>

namespace Ui
{
class UseRokuDialog;
}

class UseRokuDialog : public QDialog
{
  Q_OBJECT

 public:
  explicit UseRokuDialog(QWidget* parent = 0);
  ~UseRokuDialog();
  void set_roku(Roku* roku);

 private slots:
  void on_channelList_doubleClicked(const QModelIndex& index);

 private:
  Ui::UseRokuDialog* ui;
  Roku* roku;

  bool eventFilter(QObject* obj, QEvent* ev);

 protected:
  void keyPressEvent(QKeyEvent* ev);
};

#endif  // USEROKUDIALOG_H
