#ifndef SCANDIALOG_H
#define SCANDIALOG_H

#include "roku.h"

#include <QDialog>
#include <QUdpSocket>
#include <QByteArray>
#include <QTimer>
#include <QMap>
#include <QXmlStreamReader>
#include <QNetworkReply>

namespace Ui {
class ScanDialog;
}

class ScanDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ScanDialog(QWidget *parent = 0);
    ~ScanDialog();
    QMap<QString, Roku*> get_rokus();

private:
    Ui::ScanDialog *ui;
    QUdpSocket* udpSock;
    QTimer* timer;
    QMap<QString, Roku*> rokus;
    QNetworkAccessManager qnam;
    QNetworkReply* reply;

    void do_ssdp_scan();
    void parse_ssdp_reply(QByteArray);
    QByteArray download_icon(QString ip, QString channelID, QString port);
    void download_roku_channels(QString ip, QString port);

private slots:
    void read_ssdp_reply();
    void handle_roku_channel_response(QString ip, QString port);
    void on_closeButton_clicked();
};

#endif // SCANDIALOG_H
