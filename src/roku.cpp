#include "roku.h"

#include <QByteArray>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QXmlStreamReader>

Roku::Roku(QString ip, QString port)
{
  this->ip = ip;
  this->port = port;

  this->download_channels();
}

Roku::Roku() { this->ip = "0.0.0.0"; }

void Roku::add_channel(RokuChannel channel) { this->channels.append(channel); }

QVector<RokuChannel> Roku::get_channels() { return this->channels; }

bool Roku::has_channel(QString name)
{
  unsigned int size = this->channels.size();
  for (unsigned int k = 0; k < size; k++)
  {
    if (this->channels[k].get_name() == name)
    {
      return true;
    }
  }

  return false;
}

QString Roku::get_ip() { return this->ip; }

bool Roku::launch(QString name)
{
  foreach (RokuChannel channel, this->channels)
  {
    if (channel.get_name() == name)
    {
      QString s = QString("http://%1:%2/launch/%3");
      QUrl url(s.arg(this->ip, this->port, channel.get_attribute("id")));
      QNetworkRequest req(url);
      QByteArray data;
      this->reply = this->qnam.post(req, data);

      this->reply->waitForReadyRead(2000);

      if (this->reply->error())
      {
        return false;
      }

      return true;
    }
  }
  return false;
}

void Roku::up()
{
  QString s = QString("http://%1:%2/keypress/Up");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::down()
{
  QString s = QString("http://%1:%2/keypress/Down");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::left()
{
  QString s = QString("http://%1:%2/keypress/Left");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::right()
{
  QString s = QString("http://%1:%2/keypress/Right");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::select()
{
  QString s = QString("http://%1:%2/keypress/Select");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::character(QString c)
{
  QString s = QString("http://%1:%2/keypress/Lit_%3");
  QUrl url(s.arg(this->ip, this->port, c));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::back()
{
  QString s = QString("http://%1:%2/keypress/Back");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::backspace()
{
  QString s = QString("http://%1:%2/keypress/Backspace");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::pause()
{
  QString s = QString("http://%1:%2/keypress/Play");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::rev()
{
  QString s = QString("http://%1:%2/keypress/Rev");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::fwd()
{
  QString s = QString("http://%1:%2/keypress/Fwd");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::home()
{
  QString s = QString("http://%1:%2/keypress/Home");
  QUrl url(s.arg(this->ip, this->port));
  QNetworkRequest req(url);
  QByteArray data;
  this->qnam.post(req, data);
}

void Roku::download_channels()
{
  QString s = QString("http://%1:%2/query/apps").arg(this->ip, this->port);
  QUrl url(s);
  QNetworkRequest req(url);
  this->reply = this->qnam.get(req);
  connect(this->reply, &QNetworkReply::finished, this,
          [this] { handle_channel_response(); });
}

QByteArray Roku::download_channel_icon(QString channelID)
{
  QString iconString = QString("http://%1:%2/query/icon/%3")
                           .arg(this->ip, this->port, channelID);

  // Setting up download objects
  QUrl url(iconString);
  QNetworkRequest req(url);
  QNetworkReply* rep;

  // Use QEventLoop to synchronously wait for the data
  QEventLoop lp;
  rep = this->qnam.get(req);
  connect(rep, SIGNAL(finished()), &lp, SLOT(quit()));
  lp.exec();

  // Now we have the icon
  QByteArray qba = rep->readAll();

  /*
    QFile f(QString("/home/chris/sandbox/imgs/%1.jpg").arg(channelID));
    f.open(QIODevice::WriteOnly);
    f.write(qba);
    f.close();
  */

  return qba;
}

void Roku::handle_channel_response()
{
  qDebug() << "Handling network reply from" << this->ip;

  if (this->reply->error())
  {
    qDebug() << "Error: " << this->reply->error();
    return;
  }

  QByteArray data = this->reply->readAll();

  QXmlStreamReader reader;
  reader.addData(data);

  while (reader.readNextStartElement())
  {
    // Found the "apps" section
    if (reader.name() == "apps")
    {
      // For each app in the list
      while (reader.readNextStartElement())
      {
        QMap<QString, QString> attributes;
        foreach (QXmlStreamAttribute attr, reader.attributes().toList())
        {
          attributes.insert(attr.name().toString(), attr.value().toString());
        }

        QString name = reader.readElementText();
        QByteArray icon = this->download_channel_icon(attributes["id"]);
        RokuChannel channel(name, attributes, icon);
        this->channels.append(channel);
      }
    }
  }
}
