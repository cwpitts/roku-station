#ifndef SSDP_H
#define SSDP_H

#include <QString>

namespace ssdp
{
    quint16 PORT = 1900;
    QString IP("239.255.255.250");
    QString REQUEST("M-SEARCH * HTTP/1.1\nHost: 239.255.255.250:1900\nMan: ssdp:discover\nST: roku:ecp\n");
    QString LOCATION_HEADER("LOCATION: ");
}

#endif // SSDP_H
