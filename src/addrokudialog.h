#ifndef ADDROKUDIALOG_H
#define ADDROKUDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class AddRokuDialog;
}

class AddRokuDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddRokuDialog(QWidget *parent = 0);
    ~AddRokuDialog();
    QString get_ip();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::AddRokuDialog *ui;
    QString ip;
};

#endif // ADDROKUDIALOG_H
