#include "mainwindow.h"
#include "addrokudialog.h"
#include "chooserokudialog.h"
#include "constants.h"
#include "ui_mainwindow.h"
#include "userokudialog.h"

#include <QApplication>
#include <QDebug>
#include <QEventLoop>
#include <QKeyEvent>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QUrl>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->setWindowTitle("Roku Station");
  this->crd = new ChooseRokuDialog(this);
  this->ard = new AddRokuDialog(this);
  this->scnd = new ScanDialog(this);

  this->scnd->exec();

  QMap<QString, Roku*> discovered = this->scnd->get_rokus();
  for (Roku* r : discovered)
  {
    qDebug() << r->get_ip();
  }

  this->rokus = discovered;
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_addRokuButton_clicked()
{
  if (this->ard->exec())
  {
    QString addr = this->ard->get_ip();
    QStringList parts = addr.split(':');

    if (parts.size() == 2)
    {
      this->rokus[parts[0]] = new Roku(parts[0], parts[1]);
    }
    else if (parts.size() == 1)
    {
      this->rokus[parts[0]] = new Roku(parts[0], DEFAULT_ROKU_PORT);
    }
  }
}

void MainWindow::on_chooseRokuButton_clicked()
{
  this->crd->set_roku_list(this->rokus.keys());
  if (!this->crd->exec())
  {
    return;
  }

  this->urd = new UseRokuDialog(this);
  this->urd->set_roku(this->rokus[this->crd->get_chosen_roku()]);
  if (!this->urd->exec())
  {
    return;
  }
}
