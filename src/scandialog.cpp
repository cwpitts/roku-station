#include "scandialog.h"
#include "roku.h"
#include "ssdp.h"
#include "ui_scandialog.h"

#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QNetworkDatagram>
#include <QString>
#include <QUdpSocket>

ScanDialog::ScanDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::ScanDialog)
{
  ui->setupUi(this);
  this->ui->closeButton->hide();
  this->setWindowTitle("Roku Station | Scanning");
  this->udpSock = new QUdpSocket(this);
  this->ui->statusBar->setAlignment(Qt::AlignCenter);

  this->do_ssdp_scan();
}

ScanDialog::~ScanDialog() { delete ui; }

void ScanDialog::parse_ssdp_reply(QByteArray data)
{
  this->ui->statusBar->setText("Parsing SSDP reply");
  QMap<QString, QString> reply;

  QStringList lines = QString(data).split("\r\n");
  for (QString line : lines)
  {
    QStringList parts = line.split(":");

    // Parse into dictionary, ignoring HTTP header
    if (!parts[0].contains("HTTP"))
    {
      QString key = parts[0].toLower();
      parts.removeAt(0);
      reply[key] = parts.join(':');
    }
  }

  QStringList locationParts = reply["location"].remove("http://").split(':');
  if (reply.contains("server") && reply["server"].toLower().contains("roku"))
  {
    this->ui->statusBar->setText("Found Roku station at " + reply["location"]);

    QString ip = locationParts[0].remove(" ");

    this->rokus.insert(ip, new Roku(ip, locationParts[1]));

    this->ui->statusBar->setText("Finished adding Roku device");
    this->ui->progressWindow->append("Found Roku device at " +
                                     locationParts[0]);
    this->ui->closeButton->show();
  }
  else
  {
    this->ui->statusBar->setText("Skipping non-Roku device");
    this->ui->progressWindow->append("Skipping non-Roku device at " +
                                     locationParts[0]);
  }
}

void ScanDialog::do_ssdp_scan()
{
  this->ui->statusBar->setText("Starting SSDP scan");
  if (this->udpSock == NULL)
  {
    this->udpSock = new QUdpSocket(this);
  }

  const QByteArray data(ssdp::REQUEST.toUtf8());
  const QHostAddress destaddr(ssdp::IP);
  this->udpSock->writeDatagram(data, destaddr, ssdp::PORT);
  this->ui->statusBar->setText("SSDP broadcast sent");

  connect(udpSock, SIGNAL(readyRead()), this, SLOT(read_ssdp_reply()));
}

void ScanDialog::read_ssdp_reply()
{
  this->ui->statusBar->setText("Received SSDP reply");
  while (this->udpSock->hasPendingDatagrams())
  {
    QNetworkDatagram dg = this->udpSock->receiveDatagram();
    this->parse_ssdp_reply(dg.data());
  }
}

QMap<QString, Roku*> ScanDialog::get_rokus() { return this->rokus; }

void ScanDialog::download_roku_channels(QString ip, QString port)
{
  this->ui->statusBar->setText("Downloading channels");
  QString s = QString("http://%1:%2/query/apps").arg(ip, port);
  QUrl url(s);
  QNetworkRequest req(url);
  this->reply = this->qnam.get(req);
  connect(this->reply, &QNetworkReply::finished, this,
          [this, ip, port] { handle_roku_channel_response(ip, port); });
}

QByteArray ScanDialog::download_icon(QString ip, QString channelID,
                                     QString port)
{
  this->ui->statusBar->setText("Downloading icon for " + channelID);
  QString iconString =
      QString("http://%1:%2/query/icon/%3").arg(ip, port, channelID);

  // Setting up download objects
  QUrl url(iconString);
  QNetworkRequest req(url);
  QNetworkReply* rep;

  // Use QEventLoop to synchronously wait for the data
  QEventLoop lp;
  rep = this->qnam.get(req);
  connect(rep, SIGNAL(finished()), &lp, SLOT(quit()));
  lp.exec();

  // Now we have the icon
  QByteArray qba = rep->readAll();

  /*
  QFile f(QString("/home/chris/sandbox/imgs/%1.jpg").arg(channelID));
  f.open(QIODevice::WriteOnly);
  f.write(qba);
  f.close();
  */

  this->ui->statusBar->setText("Downloaded icon for channel " + channelID);

  return qba;
}

void ScanDialog::handle_roku_channel_response(QString ip, QString port)
{
  this->ui->statusBar->setText("Handling network reply from " + ip);
  if (this->reply->error())
  {
    qDebug() << "Error: " << this->reply->error();
    return;
  }

  QByteArray data = this->reply->readAll();

  QXmlStreamReader reader;
  reader.addData(data);
  Roku* roku = new Roku(ip, port);

  while (reader.readNextStartElement())
  {
    // Found the "apps" section
    if (reader.name() == "apps")
    {
      // For each app in the list
      while (reader.readNextStartElement())
      {
        QMap<QString, QString> attributes;
        foreach (QXmlStreamAttribute attr, reader.attributes().toList())
        {
          attributes.insert(attr.name().toString(), attr.value().toString());
        }

        QString name = reader.readElementText();
        QByteArray icon = this->download_icon(ip, attributes["id"], port);
        RokuChannel channel(name, attributes, icon);
        roku->add_channel(channel);
      }
    }
  }

  this->rokus.insert(ip, roku);
  this->ui->statusBar->setText("Finished adding Roku device");
  this->ui->progressWindow->append("Found Roku device at " + ip);
  this->ui->closeButton->show();
}

void ScanDialog::on_closeButton_clicked() { this->close(); }
