cmake_minimum_required(VERSION 3.6)
project(roku-station)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5 COMPONENTS Core Widgets Network REQUIRED)

set(SOURCES
  src/addrokudialog.cpp
  src/chooserokudialog.cpp
  src/main.cpp
  src/mainwindow.cpp
  src/rokuchannel.cpp
  src/roku.cpp
  src/scandialog.cpp
  src/userokudialog.cpp)

add_executable(roku-station ${SOURCES})
target_link_libraries(roku-station
  Qt5::Widgets
  Qt5::Core
  Qt5::Network)
